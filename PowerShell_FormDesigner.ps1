############################################################################################
# PowerShell Form Designer
# The prototype was "PowerShell Form Builder" by Z.Alex <https://gallery.technet.microsoft.com/scriptcenter/Powershell-Form-Builder-3bcaf2c7>
# Author: mozers�
$Version = '3.4.6'
# PowerShell 2.0 or later
# Update: https://bitbucket.org/mozers/powershell-formdesigner
#
# amended gpl 2019-11-28 - added checkbox at top of designer
#							if checked, on save will include the line
#							. .\{formname}_support.ps1
#							this allows you to have all the dynamic
#							code in an include file without having to
#							touch the generated code
#
# amended gpl 2020-03-26 - moved the list of available controls to array
#							at top of code, added ProgressBar to list and
#							moved most frequently use controls to the top
#
# amended gpl 2020-03-29 - preserve the state of the include file checkbox
#
# amended gpl 2020-04-05 - allow properties and events to be deleted from
#							the list of selected ones - just select the
#							property or event from the list a second time
#							and the button text will change to Remove (in red)
#							Note that the controls retain their propery
#							values on the designer form, but will not be
#							saved when the code is generated
#
# amended gpl 2020-04-05 - if you select multiple controls on the control
#							list a new set of buttons become available -
#							these will set each of the selected controls:
#							the left edge of each to the leftmost control
#							the right edge of each to the rightmost control
#							the top of each to the topmost control
#							the bottom of each to the bottommost control
#							the width of each to the narrowest control
#							the height of each to the shortest control
#
# amended gpl 2020-04-07 - corrected bug where if you changed the name of
#							the control, the events would still refer to
#							the old control name. The event code stored
#							replaces the control name with '#' and is
#							updated on form save with the actual (ie
#							current) control name. On load, the control
#							name is again replaced by a '#'
#
# amended gpl 2020-04-07 - Added MoveUp control to re order the controls list
#
# amended gpl 2020-04-07 - Added Clone control option to duplicate the
#
# amended gpl 2020-04-09 - Added tooltips to the buttons
#
# amended gpl 2020-04-09 - Added controls to even spacing horizontally or
#							vertically between selected controls
#
# amended gpl 2020-04-09 - Added utility to the property value selection
#							double-clicking the property name for a boolean
#							will toggle the value (true=> false and vice versa)
#							double-clicking a property name that ends in color
#							will open a colour picker note that not all shades
#							have a defined name and will return as a colour
#							value.
#							Double-clicking a property name that ends in font
#							will open a font picker note that only 1 type
#							of text decoration is possible (bold/italic etc)
#
# amended gpl 2020-04-10 - Accidentally closing the designer results in loss of
#							work added confirmation dialog
#							Tidied UI organisation of buttons
#
# amended gpl 2020-04-10 - Press shift when clicking on the align/size buttons
#							will reverse the sense of the action ie alignment
#							will be against the rightmost left edge instead or
#							leftmost left edge Sizing will be for the largest
#							value, not the smallest
#
# amended gpl 2020-04-10 - 2 new controls added - increase Horiz/Vertical
#							spacing press shift to reduce the spacing
#
# amended gpl 2020-04-11 - Events now have an option - the default is 'Default
#							Event Handler' - that is, a reference to a scriptblock
#							called ControlName_EventName however this may be edited
#							to refer to some other code, such as Set-SizeWidthValues
#							"Left" (Get-KeyState) as seen later in this script there
#							is no need to add in the scriptblock braces as these will be
#							added automatically... it probably doesnt matter if you do
#
# amended gpl 2020-04-12 - Fixed error where non-colour name values were not saved
#							correctly
#							User Guide created
#
# amended gpl 2020-04-12 - Clone control now places the control in the correct parent
#							container any control that is a container will now work
#
# amended gpl 2020-04-12 - Clone control can now clone a set of selected controls
#
# amended gpl 2020-04-13 - Corrected bug in identifying a container control so that all
#							container types can be used
#
# amended gpl 2020-04-13 - Refactored event management code in form load
#
# amended gpl 2020-04-25 - Enabled the Icon Pick dialog - my apologies to all proper
#							coders but this requires the use of embedded visual basic
#							(there were too few C# examples to make this work) - my
#							thanks to all the people who posted their code to assist
#							in this; connected to *Icon and *Image properties
#
# amended gpl 2020-04-30 - If the type of property is an Enum, or Boolean, then double-
#							clicking on the property name will show a combo, preselected
#							with the available options - it does not as of this version
#							show the current selected value
#
# amended gpl 2020-05-01 - Corrected bug that prevented the size/align etc controls from
#							being activated
#							Added Create Skeleton checkbox to create a skeleton support
#							file on save - if exists, will confirm overwrite
#							To restore an event handler to the default, delete the value
#							in the 2nd column
#
# amended gpl 2020-05-09 - Opening images - if you need to open a graphic image file
#							(eg a form background image) then at the select icon dialog,
#							click cancel - the file-open dialog will appear with the
#							various image file formats preselected. Select the wanted
#							file and click open; this will retrieve the image and add it
#							to the control. Note you can change the view of the dialog to
#							show images (icons)
#
# amended gpl 2020-12-22 - The combo for selecting True/False or Enum properties now
#							defaults to the current value
#
# amended gpl 2021-01-05 - When reordering rows in the controls list, retain the selection
#							on the 'moved' item
#
############################################################################################

##[System.Windows.Forms.MessageBox]::Show($msg)

Add-Type -AssemblyName System.Windows.Forms
Add-Type -AssemblyName System.Drawing
[Windows.Forms.Application]::EnableVisualStyles()
$Global:frmDesign = $null
$Global:CurrentCtrl = $null
$Global:Source = ''
## build up sourcecode for the _support file
$Global:SupportSource = ""

## used to monitor a popup combo with enum values
$Global:EnumSelected = $False

$Global:iCurFirstX = 0
$Global:iCurFirstY = 0
$Global:AvailableControls = @(
								"Label",
								"TextBox",
								"Button",
								"CheckBox",
								"ComboBox",
								"DataGridView",
								"DateTimePicker",
								"GroupBox",
								"ListBox",
								"ListView",
								"RadioButton",
								"PictureBox",
								"ProgressBar",
								"RichTextBox",
								"TreeView",
								"Chart",
								"CheckedListBox",
								"ContextMenuStrip",
								"DomainUpDown",
								"DropDownItems",
								"FlowLayoutPanel",
								"HelpProvider",
								"ImageList",
								"LinkLabel",
								"MaskedTextBox",
								"MenuStrip",
								"MonthCalendar",
								"NotifyIcon",
								"NumericUpDown",
								"Panel",
								"PropertyGrid",
								"RadioButton",
								"SplitContainer",
								"StatusStrip",
								"TabControl",
								"TableLayoutPanel",
								"ToolStripContainer",
								"ToolTip",
								"TrackBar",
								"WebBrowser"
								)

# region Icon Management

Function Convert-Base64ToImage ($ImageString) {
## This function retrieves a string that contains a
## base64 encoded image and converts back to a binary image

	$iconBytes       = [Convert]::FromBase64String($ImageString)
	$stream          = New-Object IO.MemoryStream($iconBytes, 0, $iconBytes.Length)
	$stream.Write($iconBytes, 0, $iconBytes.Length);
	$iconImage       = [System.Drawing.Image]::FromStream($stream, $true)

	Return [System.Drawing.Icon]::FromHandle((New-Object System.Drawing.Bitmap -Argument $stream).GetHIcon())
}

#region VB code

$code = @"
Imports System.Runtime.InteropServices
Imports System.Reflection.Assembly
Imports Microsoft.VisualBasic
Imports System
Imports System.Drawing

Namespace Utility
	Public Class IconHelper
		Declare Unicode Function PickIconDlg Lib "Shell32" Alias "PickIconDlg" (ByVal hwndOwner As IntPtr, ByVal lpstrFile As String, ByVal nMaxFile As Integer, ByRef lpdwIconIndex As Integer) As Integer

		Declare Function ExtractIcon Lib "shell32.dll" Alias "ExtractIconExA" (ByVal lpszFile As String, ByVal nIconIndex As Integer, ByRef phiconLarge As Integer, ByRef phiconSmall As Integer, ByVal nIcons As Integer) As Integer
		Declare Function DrawIcon Lib "user32" (ByVal hdc As Long, ByVal x As Long, ByVal y As Long, ByVal hIcon As Long) As Long

		Public Function OpenIconDialog() As Object()
			Dim iconfile As String = Environment.GetFolderPath(Environment.SpecialFolder.System) + "\shell32.dll"
			Dim iconindex As Integer ' Will store the index of the selected icon
			Dim RetStr as string

			If PickIconDlg(Nothing, iconfile, 50, iconindex) = 1 Then
				 RetStr = GetIcon(iconfile, iconindex)
				Return {RetStr}
			Else
				Return Nothing
			End If
		End Function

		Private Function GetIcon(ByVal iconfile As String, ByVal iconindex As Integer) As String
			Dim intIcon, intIcon2 As Integer
			'Try To extract Icon at index
			ExtractIcon(iconfile, iconindex, intIcon, intIcon2, 1)
			'If this fails take the first icon
			If intIcon = 0 Then
				ExtractIcon(iconfile, 0, intIcon, intIcon2, 1)
			End If

			If intIcon <> 0 Then
				Dim objIcon As Icon
				Dim stream As New IO.MemoryStream
				objIcon = GetIconFH(New IntPtr(intIcon))

				Return GetIconAsString(objIcon)
			Else
				Return Nothing
			End If

		End Function

		Public Function GetIconAsString(ByVal MyIcon As Icon) As String
			Dim base64String As String
			Dim IconArr As Byte()

				Dim stream As New IO.MemoryStream
				MyIcon.Save(stream)
				IconArr = stream.ToArray()
				base64String = Convert.ToBase64String(IconArr, 0, IconArr.Length)

				stream.Close()
				Return base64String

		End Function

		Public Function GetBitmapAsString(ByVal MyImage As Bitmap) As String
			Dim base64String As String
			Dim ImageArr As Byte()

				Dim stream As New IO.MemoryStream
				MyImage.Save(stream, System.Drawing.Imaging.ImageFormat.Png)
				ImageArr = stream.ToArray()
				base64String = Convert.ToBase64String(ImageArr)

				stream.Close()
				Return base64String

		End Function

		Private Shared Function GetIconFH(ByVal inticon As IntPtr) As Icon
			GetIconFH = Icon.FromHandle(inticon)
		End Function

	End Class
End Namespace
"@

#endregion VB code

## instantiate object to manage icons
	Add-Type -TypeDefinition $code -Language VisualBasic -ReferencedAssemblies "system.drawing"
	$IconObj = New-Object Utility.IconHelper

Function Get-IconDialog {
	$IconStr = $IconObj.OpenIconDialog()
	if (!$IconStr) {
		$filename = GetFilename 'OpenFileDialog' -images

		$Img = [System.Drawing.Image]::FromFile($filename)

		$IconStr = $IconObj.GetBitmapAsString($Img)

	}
	return $IconStr
}


# endregion Icon Management

# region Resize and move control --------------------------------------
function mouseDown {
	$Global:iCurFirstX = ([System.Windows.Forms.Cursor]::Position.X)
	$Global:iCurFirstY = ([System.Windows.Forms.Cursor]::Position.Y)
}

function mouseMove ($mControlName) {
	$iCurPosX = ([System.Windows.Forms.Cursor]::Position.X)
	$iCurPosY = ([System.Windows.Forms.Cursor]::Position.Y)
	$iBorderWidth = ($Global:frmDesign.Width - $Global:frmDesign.ClientSize.Width) / 2
	$iTitlebarHeight = $Global:frmDesign.Height - $Global:frmDesign.ClientSize.Height - 2 * $iBorderWidth

	if ($Global:iCurFirstX -eq 0 -and $Global:iCurFirstY -eq 0){
		if ($this.Parent -ne $Global:frmDesign) {
			$GroupBoxLocationX = $this.Parent.Location.X
			$GroupBoxLocationY = $this.Parent.Location.Y
		} else {
			$GroupBoxLocationX = 0
			$GroupBoxLocationY = 0
		}
		$bIsWidthChange =	($iCurPosX - $Global:frmDesign.Location.X - $GroupBoxLocationX - $this.Location.X) -ge $this.Width
		$bIsHeightChange =	($iCurPosY - $Global:frmDesign.Location.Y - $GroupBoxLocationY - $this.Location.Y) -ge ($this.Height + $iTitlebarHeight)

		if ($bIsWidthChange -and $bIsHeightChange) {
			$this.Cursor = "SizeNWSE"
		} elseif ($bIsWidthChange) {
			$this.Cursor = "SizeWE"
		} elseif ($bIsHeightChange) {
			$this.Cursor = "SizeNS"
		} else {
			$this.Cursor = "SizeAll"
		}
	} else {
		$mDifX = $Global:iCurFirstX - $iCurPosX
		$mDifY = $Global:iCurFirstY - $iCurPosY
		switch ($this.Cursor){
			"[Cursor: SizeWE]"  {$this.Width = $this.Width - $mDifX}
			"[Cursor: SizeNS]"  {$this.Height = $this.Height - $mDifY}
			"[Cursor: SizeNWSE]"{
									$this.Width = $this.Width - $mDifX
									$this.Height = $this.Height - $mDifY
								}
			"[Cursor: SizeAll]" {
									$this.Left = $this.Left - $mDifX
									$this.Top = $this.Top - $mDifY
								}
		}
		$Global:iCurFirstX = $iCurPosX
		$Global:iCurFirstY = $iCurPosY
	}
}

function mouseUP {
	$this.Cursor = "SizeAll"
	$Global:iCurFirstX = 0
	$Global:iCurFirstY = 0
	ListProperties
}

function ResizeAndMoveWithKeyboard {
	if ($Global:CurrentCtrl) {
		if     ($_.KeyCode -eq 'Left'  -and $_.Modifiers -eq 'None')    {$Global:CurrentCtrl.Left   -= 1; $_.Handled = $true; ListProperties}
		elseif ($_.KeyCode -eq 'Left'  -and $_.Modifiers -eq 'Control') {$Global:CurrentCtrl.Width  -= 1; $_.Handled = $true; ListProperties}
		elseif ($_.KeyCode -eq 'Right' -and $_.Modifiers -eq 'None')    {$Global:CurrentCtrl.Left   += 1; $_.Handled = $true; ListProperties}
		elseif ($_.KeyCode -eq 'Right' -and $_.Modifiers -eq 'Control') {$Global:CurrentCtrl.Width  += 1; $_.Handled = $true; ListProperties}
		elseif ($_.KeyCode -eq 'Up'    -and $_.Modifiers -eq 'None')    {$Global:CurrentCtrl.Top    -= 1; $_.Handled = $true; ListProperties}
		elseif ($_.KeyCode -eq 'Up'    -and $_.Modifiers -eq 'Control') {$Global:CurrentCtrl.Height -= 1; $_.Handled = $true; ListProperties}
		elseif ($_.KeyCode -eq 'Down'  -and $_.Modifiers -eq 'None')    {$Global:CurrentCtrl.Top    += 1; $_.Handled = $true; ListProperties}
		elseif ($_.KeyCode -eq 'Down'  -and $_.Modifiers -eq 'Control') {$Global:CurrentCtrl.Height += 1; $_.Handled = $true; ListProperties}
		elseif ($_.KeyCode -eq 'Delete' -and $_.Modifiers -eq 'None')   {$_.Handled = $true; RemoveCurrentCtrl;}
	}
}

# endregion Resize and move control --------------------------------------

# region Controls ---------------------------------------
function GetCtrlType($ctrl) {
	return $ctrl.GetType().FullName -replace "System.Windows.Forms.", ""
}

function RemoveCurrentCtrl {
	$Global:frmDesign.Controls.Remove($Global:CurrentCtrl)
	$Global:CurrentCtrl = $Global:frmDesign
	ListControls
}

function SelectCurrentCtrlOnListControls {
	$dgvControls.Rows |% {if ($_.Cells[0].Value -eq $Global:CurrentCtrl.Name) {$_.Selected=$true; return}}
}

function ListControls {
	function EnumerateControls($container){
		foreach ($ctrl in $container.Controls) {
			$type = GetCtrlType $ctrl

			if (IsAContainer $ctrl "Form") {
				EnumerateControls $ctrl
			}
			$dgvControls.Rows.Add($ctrl.Name, $type, $ctrl)
		}
	}

	$dgvControls.Rows.Clear()
	$dgvControls.Rows.Add($Global:frmDesign.Name, 'Form', $Global:frmDesign)
	EnumerateControls $Global:frmDesign
	SelectCurrentCtrlOnListControls
}

function SetCurrentCtrl($arg){
	if ($arg.GetType().FullName -eq 'System.Int32') { # ?????? ?? ???? $dgvControls
		$Global:CurrentCtrl = $dgvControls.Rows[$arg].Cells[2].Value
	} else { # ?????? ?? ??????? ?? $frmDesign ??? ??????????? ?????
		$Global:CurrentCtrl = $arg
		SelectCurrentCtrlOnListControls
	}

	$tmp_back = $Global:CurrentCtrl.BackColor
	for ($i = 0; $i -lt 3; $i++){
		$Global:CurrentCtrl.BackColor = 'Yellow'
		Start-Sleep -m 100
		$Global:CurrentCtrl.BackColor = 'Red'
		Start-Sleep -m 100
	}
	$Global:CurrentCtrl.BackColor = $tmp_back

	$Global:CurrentCtrl.Focus()
	ListAvailableProperties
	ListProperties
	ListAvailableEvents
	ListEvents
}

function IsAContainer($ctl, $IgnoreType=""){
$ContainerTypes = @(
					"Form",
					"FlowLayoutPanel",
					"GroupBox",
					"Panel",
					"SplitContainer",
					"TabControl",
					"TableLayoutPanel",
					"ToolStripContainer"
					)

	$Type = GetCtrlType $ctl
	if ($Type -eq $IgnoreType) {$Type="No$IgnoreType"}
	$retval = ($ContainerTypes -contains $Type)

	return $retval
}

$btnAddControl_Click = {
	function CreateNewCtrlName($ctrl_type) {
		$arrCtrlNames = $dgvControls.Rows | % {$_.Cells[0].Value}
		$num = 0
		do {
			$NewCtrlName = $ctrl_type + $num
			$num += 1
		} while ($arrCtrlNames -contains $NewCtrlName)
		return $NewCtrlName
	}

	$ctrl_type = $cbAddControl.Items[$cbAddControl.SelectedIndex]
	$Control = New-Object System.Windows.Forms.$ctrl_type
	$Control.Name = CreateNewCtrlName $ctrl_type
	$Control.Cursor = 'SizeAll'
	if ($ctrl_type -eq 'ComboBox')    {$Control.IntegralHeight = $false}
	elseif ($ctrl_type -eq 'ListBox') {$Control.IntegralHeight = $false}
	$Control.Tag = @('Name','Left','Top','Width','Height')
	if (@('Button', 'CheckBox', 'GroupBox', 'Label', 'RadioButton') -contains $ctrl_type) {
		$Control.Text = $Control.Name
		$Control.Tag += 'Text'
	}
	$Control.Add_PreviewKeyDown({$_.IsInputKey = $true})
	$Control.Add_KeyDown({ResizeAndMoveWithKeyboard})
	$Control.Add_MouseDown({MouseDown})
	$Control.Add_MouseMove({MouseMove})
	$Control.Add_MouseUP({MouseUP})
	$Control.Add_Click({SetCurrentCtrl $this})

	$cur_ctrl_type = GetCtrlType $Global:CurrentCtrl

	if (IsAContainer $Global:CurrentCtrl "Form") {
		$Global:CurrentCtrl.Controls.Add($Control)
	} else {
		$Global:frmDesign.Controls.Add($Control)
		SetCurrentCtrl $Control
	}

	ListControls
	## gpl ##
# unselect all rows - dont work
#	$dgvControls.Rows | % {$_.Selected = $false}
#	$dgvControls.ClearSelected()
}

$dgvControls_SelectionChanged = {
## at this point, the multi selection has changed
## check how many controls are selected - if more than
## 1 enable the align/size grouping menu
	$NumSelected = $dgvControls.SelectedRows.Count
	$HasForm = $false
	$IsTop = $false

## on new form, the grid isnt fully formed, bomb out in that case
#	try {$tmp = $dgvControls.SelectedRows[$i].Cells[1].Value}
#	catch {return} ## quit

	## iterate through selected rows - is one of them the form?
	For ($i=0; $i -le $NumSelected; $i++) {
		If ($dgvControls.SelectedRows[$i].Cells[1].Value -eq "Form") {$HasForm = $true}
	}

	If ($dgvControls.CurrentRow.Index -eq 1) {$IsTop = $true}

	If (($NumSelected -le 1) -or ($HasForm)) { # normal
		Set-MultiTool $false
	}
	else { # multi-tool
		Set-MultiTool $true
	}

	If (($NumSelected -lt 1) -or ($HasForm)) { # normal
		$btnClone.Enabled = $false
	}
	else { # multi-tool
		$btnClone.Enabled = $true
	}

	If (($NumSelected -ne 1) -or ($HasForm) -or ($IsTop)) { # normal
		Set-MoveTool $false
	}
	else { # multi-tool
		Set-MoveTool $true
	}

}

# endregion Controls ---------------------------------------

# region Properties ---------------------------------------
function ListAvailableProperties {
	$cbAddProp.Items.Clear()
	$Global:CurrentCtrl | Get-Member -membertype properties | % {$cbAddProp.Items.Add($_.Name)}
	$cbAddProp.Text = ""
	$cbAddProp.SelectedIndex = -1
}

function ListProperties {
	try {$dgvProps.Rows.Clear()} catch {return}
	[array]$props = $Global:CurrentCtrl | Get-Member -membertype properties
	foreach ($prop in $props) {
		$pname = $prop.Name
		if ($Global:CurrentCtrl.Tag -contains $pname) {
			$value = $Global:CurrentCtrl.$pname
			if ($value.GetType().FullName -eq 'System.Drawing.Font') {
				$value = $value.Name + ', ' + $value.SizeInPoints + ', ' + $value.Style
			}
			$value = $value -replace 'Color \[(\w+)\]', '$1'
			$dgvProps.Rows.Add($pname, $value)
		}
	}
}

function AddProperty($prop_name){
	if ($btnAddProp.Text -eq "Remove") {
		$Global:CurrentCtrl.Tag = $Global:CurrentCtrl.Tag | Where {$_ -ne $prop_name}
		$btnAddProp.Text = "Add"
		$btnAddProp.ForeColor = "Black"
	}
	else {
		$Global:CurrentCtrl.Tag += $prop_name
	}
	ListProperties
}
# endregion Properties ---------------------------------------

# region Events ---------------------------------------
$cbAddProp_SelectedIndexChanged = {
## when the property combo changes, check if the property
## already exists if it does, change the button to say remove
	$prop_name = $cbAddProp.Text
	$Found = ($Global:CurrentCtrl.Tag | Where {$_ -eq $prop_name}).Count
	If ($Found) {
		$btnAddProp.Text = "Remove"
		$btnAddProp.ForeColor = "Red"
	}
	else {
		$btnAddProp.Text = "Add"
		$btnAddProp.ForeColor = "Black"
	}
}

## gpl ##
$dgvProps_DoubleClick = {
## you can only action on the name column

	# hide the combo until we know we need it
	# user may have initiated it, but changed their mind
	$cbEnums.Visible = $false
	$Global:EnumSelected = $False
	if ($dgvProps.CurrentCell.ColumnIndex -ne 0) {return}

	$prop_name = $dgvProps.CurrentRow.Cells[0].Value
	$value = $dgvProps.CurrentRow.Cells[1].Value

	## manage colour dialog
	if ($prop_name -like "*color") {
		$colorDialog = new-object System.Windows.Forms.ColorDialog
		$colorDialog.AllowFullOpen = $true

		if ($colorDialog.ShowDialog() -eq "OK") {
			$value ="#"
			$value += "{0:x2}" -f $colordialog.color.A
			$value += "{0:x2}" -f $colordialog.color.R
			$value += "{0:x2}" -f $colordialog.color.G
			$value += "{0:x2}" -f $colordialog.color.B

			$dgvProps.CurrentRow.Cells[1].Value = $value
		}
	}

	## manage font dialog
	if ($prop_name -like "*font") {
		$fontDialog = new-object windows.forms.fontdialog
		$fontDialog.Showcolor = $False
		$fontDialog.ShowEffects = $True
		$fontDialog.FontMustExist = $True

		if ($fontDialog.ShowDialog() -eq "OK") {
			$style = "Regular"
			If ($fontDialog.Font.Bold)		{$style = "Bold"}
			If ($fontDialog.Font.Italic)	{$style = "Italic"}
			If ($fontDialog.Font.Underline)	{$style = "Underline"}
			If ($fontDialog.Font.Strikeout)	{$style = "Strikeout"}

			$Face = $fontDialog.Font.FontFamily.Name
			$Size = $FontDialog.Font.Size

			$dgvProps.CurrentRow.Cells[1].Value = "$Face, $Size, $Style"
			$Global:CurrentCtrl.$prop_name = New-Object System.Drawing.Font($Face, $Size, [System.Drawing.FontStyle]::$style)
		}
	}

	## manage icon dialog
	if ($prop_name -like "*icon" -or $prop_name -like "*image") {
		$Base64 = Get-IconDialog
		$Global:CurrentCtrl.$prop_name = Convert-Base64ToImage $Base64
	}

## here, check if the property is an enum or a bool
## if so, show the list of values for selecting

	$PropType = $Global:CurrentCtrl.$prop_name.GetType().Name
	$PropIsEnum = $Global:CurrentCtrl.$prop_name.GetType().IsEnum

	If ($PropIsEnum) {
		$EnumValS = $Global:CurrentCtrl.$prop_name.GetType().GetEnumNames()
	}

	If ($PropType -eq "boolean") {
		$PropIsEnum = $true ## force to be treated like an enum
		$EnumValS = @("True","False")
	}

	If ($PropIsEnum) {
		# where is the associated cell? move the combo there

		$cellRect = $dgvProps.GetCellDisplayRectangle(1, $dgvProps.CurrentRow.Index, $true);

		$cbEnums.left = $cellRect.x + $dgvProps.left
		$cbEnums.top = $cellRect.y + $dgvProps.top
		$cbEnums.width = $cellRect.width
		$cbEnums.height = $cellRect.height

		$cbEnums.Items.Clear()

		# populate combo with list of legal values
		$cbEnums.Items.AddRange($EnumValS)

## find the current value and make that the currently selected one
		$cbEnums.SelectedIndex = $cbEnums.FindStringExact($dgvProps.CurrentRow.Cells[1].Value)		
		$cbEnums.Text = $dgvProps.CurrentRow.Cells[1].Value
	## ensure the change index logic will work
		$Global:EnumSelected = $False
		
		$cbEnums.BringToFront()
		$cbEnums.visible = $True

		# do nothing until global selected = true or combo goes invisible (cancelled)
		while (!$Global:EnumSelected -and $cbEnums.visible) {
			## process other system events
			[System.Windows.Forms.Application]::DoEvents()
		}

		$Global:EnumSelected = $False
		if (!$cbEnums.visible) {
			## we cancelled it
			return
		}
		$cbEnums.visible = $False
		$Global:EnumSelected = $False
		$dgvProps.CurrentRow.Cells[1].Value = $cbEnums.Items[$cbEnums.SelectedIndex]
	}

	& $dgvProps_CellEndEdit
}

## used to supply list of values for a property
$cbEnums_SelectedIndexChanged = {
	$Global:EnumSelected = $True
}

$dgvProps_CellEndEdit = {
	$prop_name = $dgvProps.CurrentRow.Cells[0].Value
	$value = $dgvProps.CurrentRow.Cells[1].Value

	$arrMatches = [regex]::matches($value, '^([\w ]+),\s*(\d+),\s*(\w+)$')
	if ($arrMatches.Success) { # Font
		foreach ($m in $arrMatches) {
			$font_name = [string]$m.Groups[1]
			$font_size = [string]$m.Groups[2]
			$font_style = [string]$m.Groups[3]
			$Global:CurrentCtrl.Font = New-Object System.Drawing.Font($font_name, $font_size, [System.Drawing.FontStyle]::$font_style)
		}
	} else {
		if ($value -eq 'True') {$value = $true}
		elseif ($value -eq 'False') {$value = $false}
		$Global:CurrentCtrl.$prop_name = $value
	}
	if ($Global:CurrentCtrl.Tag -notcontains $prop_name) {$Global:CurrentCtrl.Tag += $prop_name}
	if ($prop_name -eq 'Name') {ListControls}
	ListProperties
}

function ListAvailableEvents {
	$cbAddEvent.Items.Clear()
	$Global:CurrentCtrl | Get-Member | % {if ($_ -like '*EventHandler*') {$cbAddEvent.Items.Add($_.Name)}}
	$cbAddEvent.Text = ""
	$cbAddEvent.SelectedIndex = -1
}

function ListEvents {
	$dgvEvents.Rows.Clear()
	[array]$events = $Global:CurrentCtrl | Get-Member | ? {$_ -like '*EventHandler*'}
	foreach ($event in $events) {
		$ename = $event.Name
		if ($Global:CurrentCtrl.Tag -like "Add_$ename*") {
			$ThisEv = $Global:CurrentCtrl.Tag | Where {$_ -like "Add_$ename*"}
			$ThisHandler = ($ThisEv -split "\|")[-1]
			if (!$ThisHandler.Trim()) {$ThisHandler = "Default Event Handler"}
			$dgvEvents.Rows.Add($ename, $ThisHandler)
		}
	}
}

$dgvEvents_CellEndEdit = {
	$ename = $dgvEvents.CurrentRow.Cells[0].Value
	$Value = $dgvEvents.CurrentRow.Cells[1].Value
	if (!$Value.trim()) {
		$Value = "Default Event Handler"
	}

## now remove and re-add the values
	$Global:CurrentCtrl.Tag = $Global:CurrentCtrl.Tag | Where {$_ -notlike "Add_$ename*"}
	$Global:CurrentCtrl.Tag += "Add_$ename|$Value"
	ListEvents
}

Function Get-EventCode ($EventName) {
## used several times
## do not include the control name as it may change
## use a # char and replace it on save
	Return 'Add_' + $EventName + '|Default Event Handler'
}

$btnAddEvent_Click = {
## it the button says remove, do it
	$event = $cbAddEvent.Items[$cbAddEvent.SelectedIndex]
	$EvCode = Get-EventCode $event

	if ($btnAddEvent.Text -eq "Remove") {
		$Global:CurrentCtrl.Tag = $Global:CurrentCtrl.Tag | Where {$_ -notlike "Add_$event*"}
		$btnAddEvent.Text = "Add"
		$btnAddEvent.ForeColor = "Black"
	}
	else {
		$Global:CurrentCtrl.Tag += $EvCode
	}

	ListEvents
}

$cbAddEvent_SelectedIndexChanged = {
## when the event combo changes, check if the event already
## exists if it does, change the button to say remove
	$event = $cbAddEvent.Items[$cbAddEvent.SelectedIndex]
	$Found = ($Global:CurrentCtrl.Tag | Where {$_ -like "Add_$event*"}).Count
	If ($Found) {
		$btnAddEvent.Text = "Remove"
		$btnAddEvent.ForeColor = "Red"
	}
	else {
		$btnAddEvent.Text = "Add"
		$btnAddEvent.ForeColor = "Black"
	}
}

# endregion Events ---------------------------------------

# region file management
# --- New Form ---------------------------------------
$btnNewForm_Click = {
	$Global:frmDesign = New-Object System.Windows.Forms.Form
	$Global:frmDesign.Name = 'Form0'
	$Global:frmDesign.Text = 'Form0'
	$Global:frmDesign.Tag = @('Name','Width','Height','Text')
	$Global:frmDesign.Add_ResizeEnd({ListProperties})
	$Global:frmDesign.Add_FormClosing({$_.Cancel = $true})
	$Global:frmDesign.Show()
	$Global:CurrentCtrl = $Global:frmDesign
	ListControls
	ListAvailableProperties
	ListProperties
	ListAvailableEvents
	ListEvents
	EnableButtons
}

# --- Save Form ---------------------------------------

	function GetFilename($dlg_name,[switch] $Images)  {
	$Dialog = New-Object System.Windows.Forms.$dlg_name
	if ($Images) {
		$Filter = "Image files|*.jpg;*.jpeg;*.jpe;*.jfif;*.tiff;*.tif;*.png;*.gif;*.bmp|All files (*.*)|*.*"
	}
	else {
		$Filter = "Powershell Script (*.ps1)|*.ps1|All files (*.*)|*.*"
	}

	$Dialog.Filter = $Filter
	$Dialog.ShowDialog() | Out-Null
	return $Dialog.filename
}

$btnSaveForm_Click = {
	$SupportFilename = "$($($Global:frmDesign).Name)_support.ps1"
	$Global:SupportSource = ""
	$newline = "`r`n"

	function EnumerateSaveControls ($container){
		$Global:Source += '#' + $newline + '#' + $container.Name + $newline + '#' + $newline
		$ctrl_type = GetCtrlType $container
		$Global:Source += '$' + $container.Name + ' = New-Object System.Windows.Forms.' + $ctrl_type + $newline
		$left = 0; $top = 0; $width = 0; $height = 0;
		[array]$props = $container | Get-Member -membertype properties
		foreach ($prop in $props) {
			$pname = $prop.Name
			if ($container.Tag -contains $pname -and $pname -ne "Name") {
				if     ($pname -eq "Left")   {$left = $container.Left}
				elseif ($pname -eq "Top")    {$top = $container.Top}
				elseif ($pname -eq "Width")  {$width = $container.Width}
				elseif ($pname -eq "Height") {$height = $container.Height}
				else {
					$value = $container.$pname
					if ($value.GetType().FullName -eq 'System.Drawing.Font') {
						$font_name = $value.Name
						$font_size = $value.SizeInPoints
						$font_style = $value.Style
						$value = 'New-Object System.Drawing.Font("' + $font_name + '", ' + $font_size + ', [System.Drawing.FontStyle]::' + $font_style + ")"
					} else {
						if ($pname -like "*color") {
							## test, is string starting "color" ? Color [A=255, R=128, G=128, B=255]
							if ($value -like "color*") {
								$ColourArray = ($value -replace 'Color \[(.+)\]', '$1') -split ","  # trim off brackets etc and make an array
								$value ="#"
								foreach ($code in $ColourArray) {
									[int]$num = ($code -split "=")[-1]
									$value += '{0:x2}' -f $num
								}
							}
						}

						if ($pname -like "*icon" -or $pname -like "*image") {
							if ($pname -like "*icon") {
								$IconStr = $IconObj.GetIconAsString($Value)
							}
							else {
								$IconStr = $IconObj.GetBitmapAsString($Value)
							}
							$value = 'Convert-Base64ToImage "' + $IconStr + '"'
						}

						$value = $value -replace 'True', '$true' -replace 'False', '$false'
						if ($value -ne '$true' -and $value -ne '$false' -and $value -notlike 'Convert-Base64ToImage*') {$value = '"' + $value + '"'}
					}
					$Global:Source += '$' + $container.Name + '.' + $pname + ' = ' + $value + $newline
				}
			}
		}

		foreach ($event in ($container.Tag | where {$_ -like "Add_*"})) {
		## gpl ## save events - build up the actual event handler code

			$EvArr = $event -split "\|"
			$EvName = $EvArr[0]
			$EvCode = $EvArr[-1]
			$Global:Source += '$' + $container.Name + '.' + $EvName + '('
			if ($EvCode.Trim() -eq 'Default Event Handler') {
				$Global:Source += '$' + $container.Name + '_' + $EvName.substring(4)
				$Global:SupportSource += "`n" + '$' + $container.Name + '_' + $EvName.substring(4) + " = {`n`#Event handler code`n`n}`n"
			}
			else {
				$Global:Source += '{' + $EvCode + '}'
				$EvCode = ($EvCode.Trim() -split " ")[0]
				if ($Global:SupportSource -notlike "*Function " + $EvCode + " {*") {$Global:SupportSource += "`nFunction $EvCode {`n`#Event function code`n`n}`n"}
			}
			$Global:Source += ')' + $newline
		}

		if ($ctrl_type -eq 'Form') { # --- Form ----------
			$width = $container.ClientSize.Width
			$height = $container.ClientSize.Height
			$Global:Source += '$' + $container.Name + '.ClientSize = New-Object System.Drawing.Size(' + $width + ', ' + $height + ')' + $newline
		} else { # ------------ Other controls ------------
			if ($width -ne 0 -and $height -ne 0) {
				$Global:Source += '$' + $container.Name + '.Size = New-Object System.Drawing.Size(' + $width + ', ' + $height + ')' + $newline
			}

			$Global:Source += '$' + $container.Name + '.Location = New-Object System.Drawing.Point(' + $left + ', ' + $top + ')' + $newline
			$Global:Source += '$' + $container.Parent.Name + '.Controls.Add($' + $container.Name + ')' + $newline
		}
		# ------------ Containers ------------

		if (IsAContainer $container) {
			foreach ($ctrl in $container.Controls) {
				EnumerateSaveControls $ctrl
			}
		}
	}

	$Global:Source  = 'Add-Type -AssemblyName System.Windows.Forms' + $newline
	$Global:Source += 'Add-Type -AssemblyName System.Drawing' + $newline
	$Global:Source += '[Windows.Forms.Application]::EnableVisualStyles()' + $newline
	$Global:Source += $newline + 'Function Convert-Base64ToImage {' + $newline + ${function:Convert-Base64ToImage} + '}' + $newline +$newline

	## gpl - include an 'include' line if requested
	if ($cbInclude.checked) {$Global:Source += ". `".\$SupportFilename`"" + $newline}
	$Global:SupportSource += "# Skeleton support file for $filename `n`n"
	EnumerateSaveControls $Global:frmDesign

	$Global:Source += $newline + '[void]$' + $Global:frmDesign.Name + '.ShowDialog()' + $newline
	$filename = ''

	$filename = GetFilename 'SaveFileDialog'
	if ($filename -notlike '') {
		$Global:Source > $filename
		if ($cbSkeleton.checked) {
			$CanOverWrite=$True
			if (Test-Path (Join-Path (Split-Path $filename) $SupportFilename)) {
			   $result = [System.Windows.Forms.MessageBox]::Show(`
					"Overwrite support file?", `
					"Overwrite", [System.Windows.Forms.MessageBoxButtons]::YesNoCancel)
				if ($result -ne [System.Windows.Forms.DialogResult]::Yes) {
					$CanOverWrite = $false
				}
			}
			if ($CanOverWrite) {
				$Global:SupportSource = "# Skeleton support file for $filename `n`n" + 	$Global:SupportSource
				$Global:SupportSource | Out-File (Join-Path (Split-Path $filename) $SupportFilename)
			}
		}
	}
}

# --- Open Exist Form ---------------------------------------
$btnOpenForm_Click = {
	function SetControlTag($ctrl){ # ??????? ???????? ? ??????? ????????, ??????? ???? ?????? ? ???? ? ????????? ?? ? $ctrl.Tag
		$pattern = '(.*)\$' + $ctrl.Name + '\.(?:(\w+)\s*=|(Add_[^\r\n]+))'
		$arrMatches = [regex]::matches($Global:Source, $pattern)
		$arrTags = @()
		foreach ($m in $arrMatches) {
			[string]$comment = $m.Groups[1]
			if (-not $comment.Contains('#')) {
				$prop_name = [string]$m.Groups[2]
				if ($prop_name) {
					if ($prop_name -eq 'Location')                                 {$arrTags += @('Left','Top')}
					elseif ($prop_name -eq 'Size' -or $prop_name -eq 'ClientSize') {$arrTags += @('Width','Height')}
					else {$arrTags += $prop_name}
				}
				$event_handler = [string]$m.Groups[3]
				if ($event_handler) {
					## gpl ## handler looks like add_$ev(handler)  where handler is '$'$ctl_$ev or {scriptblock}
					$ActualEvent = $event_handler.split("_\(")[1] # everything between _ and (
					## process the code back to the generic one - the handler code goes after a |
					$CodeBit = ($event_handler -split '\(')[0] + '|'## everything before ( - 'add_evname' plus '|'
					## Now - is it the default handler or not?
					$Handler = $event_handler -replace ".+\((.+)\)",'$1' ## pull out the code between ()
					If ($Handler -eq "`$$($ctrl.Name)_$ActualEvent") {
						$CodeBit += "Default Event Handler"
					}
					else {
						## need to pull out a script block
						$CodeBit += $event_handler -replace ".+\{(.+)\}.+",'$1'
					}
					$arrTags += $CodeBit
				}
			}
		}
		if ($arrTags -notcontains 'Name') {$arrTags += 'Name'}
		$ctrl.Tag = $arrTags
	}

	function EnumerateLoadControls($container) {
		foreach ($ctrl in $container.Controls) {
			SetControlTag $ctrl
			$ctrl_type = GetCtrlType $ctrl
			if (IsAContainer $ctrl) {
				EnumerateLoadControls $ctrl
			}
			if ($ctrl_type -eq 'ComboBox')    {$ctrl.IntegralHeight = $false}
			elseif ($ctrl_type -eq 'ListBox') {$ctrl.IntegralHeight = $false}
			elseif ($ctrl_type -ne 'WebBrowser') {
				$ctrl.Cursor = 'SizeAll'
				$ctrl.Add_PreviewKeyDown({$_.IsInputKey = $true})
				$ctrl.Add_KeyDown({ResizeAndMoveWithKeyboard})
				$ctrl.Add_MouseDown({MouseDown})
				$ctrl.Add_MouseMove({MouseMove})
				$ctrl.Add_MouseUP({MouseUP})
				$ctrl.Add_Click({SetCurrentCtrl $this})
			}
		}
	}

	$filename = GetFilename 'OpenFileDialog'
	if ($filename -notlike ''){
		$Global:Source = get-content $filename | Out-String
		## gpl ## check if theres an include included
		$cbInclude.checked = $Global:Source -like '*. ".\*_support.ps1*'
		# ?????? ?????? ???? - ????? ?????????? ?????
		$pattern = '(.*)\$(\w+)\s*=\s*New\-Object\s+(System\.)?Windows\.Forms\.Form'
		$arrMatches = [regex]::matches($Global:Source, $pattern)
		foreach ($m in $arrMatches) {
			[string]$comment = $m.Groups[1]
			if (-not $comment.Contains('#')) {
				$form_name = $m.Groups[2]
			}
		}
		if ($form_name) { # ???? ? ?????? ??????? ????? ??? ?????
			$find = '\$' + $form_name + '\.Show(Dialog)?\(\)' ## show or showdialog
			$Global:Source = $Global:Source -replace $find, '' # ??????? ?????? ?? ??????? ## remove the show / showdialog command

			Invoke-Expression -Command $Global:Source # ?????????? ???? ??????????? ?????

			try {$Global:frmDesign = Get-Variable -ValueOnly $form_name} catch {} # ???? ? ?????????? PowerShell ??? ?????
			if ($Global:frmDesign) {
				# ????????? ???? ????????? ?? ????? property Name, ?????? ????? ??????????, ???????? ??
				Get-Variable | where {[string]$_.Value -like 'System.Windows.Forms.*'} | where {try {$_.Value.Name = $_.Name} catch {}}

				# ?????? ?????? ???????? ? ?????? ???? ????????? ?? ?????
				EnumerateLoadControls $Global:frmDesign
				# ? ????? ????? ????
				$Global:frmDesign.Name = $form_name
				SetControlTag $Global:frmDesign
				$Global:frmDesign.Add_ResizeEnd({ListProperties})
				$Global:frmDesign.Add_FormClosing({$_.Cancel = $true})
				$Global:frmDesign.Show()

				$Global:CurrentCtrl = $Global:frmDesign
				ListControls
				ListAvailableProperties
				ListProperties
				ListAvailableEvents
				ListEvents
				EnableButtons
			} else {
				$message = "Can't find variable $" + $form_name + "`nPlease open ONLY SOURCE OF FORM.`nExclude other code."
				[System.Windows.Forms.MessageBox]::Show($message, 'Error to open exist Form', 'OK', 'Error')
			}
		} else {
			$message = 'Your code does not contain any form!'
			[System.Windows.Forms.MessageBox]::Show($message, 'Error to open exist Form', 'OK', 'Error')
		}
	}
}

# endregion file management

# region align/size controls

Function Collect-SelectedDetails {

	$PropsHash = @{
				"Name" =	""
				"Control" =	$null
				"Type" =	""
				"Top" = 	0
				"Left" =	0
				"Right" = 	0
				"Bottom" =	0
				"Height" = 	0
				"Width" =	0
			}

	$Controls = @()

	$NumSelected = $dgvControls.SelectedRows.Count

	## build an array of objects holding the info about the controls
	For ($i=0; $i -lt $NumSelected; $i++) {

		$PropsHash.Name = $dgvControls.SelectedRows[$i].Cells[0].Value
		$PropsHash.Type = $dgvControls.SelectedRows[$i].Cells[1].Value
		$PropsHash.Control = $dgvControls.SelectedRows[$i].Cells[2].Value

		$ThisCtl = $dgvControls.SelectedRows[$i].Cells[2].Value

		Foreach ($Prop in @("Top","Left","Height","Width")) {
			$PropsHash.$Prop = $ThisCtl.$Prop
		}
		## store a couple of derived properties
		$PropsHash.Right = $ThisCtl.Left + $ThisCtl.Width
		$PropsHash.Bottom = $ThisCtl.Top + $ThisCtl.Height

		$Controls += New-Object -TypeName PSObject -property $PropsHash
	}

	Return $Controls
}

function EnableButtons{
	$btnSaveForm.Enabled = $true
	$btnAddControl.Enabled = $true
	$btnRemoveControl.Enabled = $true
	$btnAddProp.Enabled = $true
	$btnNewForm.Enabled = $false
	$btnOpenForm.Enabled = $false
	$btnAddEvent.Enabled = $true
}

Function CloneControl {

	foreach ($CurrCtl in Collect-SelectedDetails) {
	# store the reference to the currently selected control
		$CurrentCtl =	$CurrCtl.Control
		$CurrentType =	$CurrCtl.Type
		$CurrentName =	$CurrCtl.Name

		$Control = New-Object System.Windows.Forms.$CurrentType

		#get props and assign to new control
		$AllProps = $CurrentCtl.Tag | Where {($_ -notlike "Add_*") -and (@("Name","Tag") -notcontains $_)}
		foreach ($Prop in $AllProps) {
			$Control.$Prop = $CurrentCtl.$Prop
		}

		$Control.Tag = $CurrentCtl.Tag

		## make a new unique name
		$Suffix = 1
		$NewName = "$($CurrentName)_$Suffix"
		$AllNames = $dgvControls.Rows | % {$_.Cells[0].Value}
		While ($AllNames -contains $NewName) {
			$Suffix++
			$NewName = "$($CurrentName)_$Suffix"
		}
		$Control.Name = $NewName

		$Control.Cursor = 'SizeAll'
		$Control.Add_PreviewKeyDown({$_.IsInputKey = $true})
		$Control.Add_KeyDown({ResizeAndMoveWithKeyboard})
		$Control.Add_MouseDown({MouseDown})
		$Control.Add_MouseMove({MouseMove})
		$Control.Add_MouseUP({MouseUP})
		$Control.Add_Click({SetCurrentCtrl $this})

		$Parent = $CurrentCtl.Parent

		## form or container, add it in the right place
		$Parent.Controls.Add($Control)
	}
	ListControls
}

Function MoveRow ($Direction) {
	$Tmp=$null
	$CurrRow = $dgvControls.CurrentRow.Index
	# swap with row above
	foreach ($ndx in (0..2)) {
		$Tmp = $dgvControls.Rows[$CurrRow -1].Cells[$ndx].Value
		$dgvControls.Rows[$CurrRow -1].Cells[$ndx].Value = $dgvControls.Rows[$CurrRow].Cells[$ndx].Value
		$dgvControls.Rows[$CurrRow].Cells[$ndx].Value = $Tmp
	}
	
	# set the curr control to be the one in the line above (ie the old current one)
	$dgvControls.Rows[$CurrRow].Selected = $false
	$dgvControls.CurrentCell = $dgvControls.Rows[$CurrRow -1].Cells[0]
	$dgvControls.Rows[$CurrRow -1].Selected = $true
}

Function Set-SizeWidthValues ($Prop, $Key) {
# Right and bottom require the max value
	$GetMin = @("Left", "Top", "Height","Width") -contains $Prop

	$Controls = Collect-SelectedDetails

	$SortedControls = $Controls | Sort $Prop,Name
	$First = $SortedControls[0]
	$Last = $SortedControls[-1]

	if ($Key) {$First,$Last = $Last,$First} ## swap low/high values

	foreach ($Control in $Controls) {
		$ThisCtl = $Control.Control
		if ($GetMin) {
			$ThisCtl.$Prop = $First.$Prop
		}
		else {
			if ($Prop -eq "Bottom") {
				$ThisCtl.Top = $Last.Bottom - $ThisCtl.Height
			}
			else { # right
				$ThisCtl.Left = $Last.Right - $ThisCtl.Width
			}
		}
	}

}

Function Set-MultiTool ($State) {

	$btnAddEvent.Enabled = !$State
	$btnAddProp.Enabled = !$State

	$btnAlignBottom.Enabled = $State
	$btnAlignLeft.Enabled = $State
	$btnAlignRight.Enabled = $State
	$btnAlignTop.Enabled = $State
	$btnSizeHeight.Enabled = $State
	$btnSizeWidth.Enabled = $State

	$btnSpaceHorizontal.Enabled = $State
	$btnSpaceVertical.Enabled = $State
	$btnMoreWidth.Enabled = $State
	$btnMoreHeight.Enabled = $State
}

Function Set-MoveTool ($State) {
	$btnMoveUp.Enabled = $State
}

Function Space-Control ($Direction, $Key, $Extra) {

	$SortKey = ""
	$AdjustVal = ""
	if ($Direction -eq "Vertical") {
		$SortKey = "Top"
		$AdjustVal = "Height"
	}
	Else {
		$SortKey = "Left"
		$AdjustVal = "Width"
	}
	
	if ($Key) {$Extra = -$Extra}

	$NumSelected = $dgvControls.SelectedRows.Count

	$Controls = Collect-SelectedDetails

	$SortedControls = $Controls | Sort $SortKey,Name
	$First = $SortedControls[0]
	$Last = $SortedControls[-1]
	$Last.$SortKey += $Extra
	($Last.Control).$SortKey += $Extra

	if ($Key) {$First,$Last = $Last,$First} ## swap low/high values

	$FreeSpace = $Last.$SortKey - $First.$SortKey
	Foreach ($Ndx in (0..($NumSelected -2))) {
		## subtract the space used up by the intermediate controls
		$FreeSpace -= ($SortedControls[$Ndx]).$AdjustVal
	}

	## the space needs to be distributed evenly between the controls between first and last

	$Gap = $FreeSpace / ($NumSelected -1)
	$OldEdge = $First.$SortKey + $First.$AdjustVal
	Foreach ($Ndx in (1..($NumSelected -2))) {
		$ThisCtl = ($SortedControls[$Ndx]).Control

		$ThisCtl.$SortKey = $OldEdge + $Gap
		$OldEdge = $OldEdge + $Gap + $ThisCtl.$AdjustVal
	}

}

# endregion align/size controls

$frmPSFD_Closing = {
param($sender,$e)

    $result = [System.Windows.Forms.MessageBox]::Show(`
        "Are you sure you want to exit?", `
        "Close", [System.Windows.Forms.MessageBoxButtons]::YesNoCancel)
    if ($result -ne [System.Windows.Forms.DialogResult]::Yes) {
        $e.Cancel= $true
    }
	$cbEnums.visible = $False # should clear any outstanding event
	
}

function Get-KeyState([uint16]$keyCode = 0x10) {
   $signature = '[DllImport("user32.dll")]public static extern short GetKeyState(int nVirtKey);'
   $type = Add-Type -MemberDefinition $signature -Name User32 -Namespace GetKeyState -PassThru
   return [bool]($type::GetKeyState($keyCode) -band 0x80)
 }


# --- Show Main Window ---------------------------------------
function ShowMainWindow {
	#
	#frmPSFD
	#
	$frmPSFD = New-Object System.Windows.Forms.Form
	$frmPSFD.ClientSize = New-Object System.Drawing.Size(549, 564)
	$frmPSFD.FormBorderStyle = 'Fixed3D'
	$frmPSFD.MaximizeBox = $false
	$frmPSFD.Add_Closing($frmPSFD_Closing)
	$frmPSFD.Text = 'PowerShell Form Designer ' + $Version
	## gpl ##
	#
	#ToolTip
	#
	$ToolTips = New-Object System.Windows.Forms.ToolTip
	$ToolTips.IsBalloon = $true

# region control controls
## align
	# --------------------------------------------------------
	#gbAlign
	#
	$gbAlign = New-Object Windows.Forms.GroupBox
	$gbAlign.Location = New-Object System.Drawing.Point(12, 2)
	$gbAlign.Size = New-Object System.Drawing.Size(190, 43)
	$gbAlign.Text = "Align:"
	$frmPSFD.Controls.Add($gbAlign)
	#
	#btnAlignLeft
	#
	$btnAlignLeft = New-Object System.Windows.Forms.Button
	$btnAlignLeft.Enabled = $false
	$btnAlignLeft.Text = "Left"
	$btnAlignLeft.Add_Click({Set-SizeWidthValues "Left" (Get-KeyState)})
	$btnAlignLeft.Size = New-Object System.Drawing.Size(44, 23)
	$btnAlignLeft.Location = New-Object System.Drawing.Point(8, 13)
	$gbAlign.Controls.Add($btnAlignLeft)
	$ToolTips.SetToolTip($btnAlignLeft, "Align selected controls to the left edge of`nthe leftmost control (press shift to`nalign to left of rightmost control)");
	#
	#btnAlignRight
	#
	$btnAlignRight = New-Object System.Windows.Forms.Button
	$btnAlignRight.Enabled = $false
	$btnAlignRight.Text = "Right"
	$btnAlignRight.Add_Click({Set-SizeWidthValues "Right" (Get-KeyState)})
	$btnAlignRight.Size = New-Object System.Drawing.Size(44, 23)
	$btnAlignRight.Location = New-Object System.Drawing.Point(52, 13)
	$gbAlign.Controls.Add($btnAlignRight)
	$ToolTips.SetToolTip($btnAlignRight, "Align selected controls to the right edge of`nthe rightmost control (press shift to`nalign to right of lefttmost control)");
	#
	#btnAlignTop
	#
	$btnAlignTop = New-Object System.Windows.Forms.Button
	$btnAlignTop.Enabled = $false
	$btnAlignTop.Text = "Top"
	$btnAlignTop.Add_Click({Set-SizeWidthValues "Top" (Get-KeyState)})
	$btnAlignTop.Size = New-Object System.Drawing.Size(44, 23)
	$btnAlignTop.Location = New-Object System.Drawing.Point(96, 13)
	$gbAlign.Controls.Add($btnAlignTop)
	$ToolTips.SetToolTip($btnAlignTop, "Align selected controls to the top edge of`nthe topmost control (press shift to`nalign to top of bottomtmost control)");
	#
	#btnAlignBottom
	#
	$btnAlignBottom = New-Object System.Windows.Forms.Button
	$btnAlignBottom.Enabled = $false
	$btnAlignBottom.Text = "Bot"
	$btnAlignBottom.Add_Click({Set-SizeWidthValues "Bottom" (Get-KeyState)})
	$btnAlignBottom.Size = New-Object System.Drawing.Size(44, 23)
	$btnAlignBottom.Location = New-Object System.Drawing.Point(140, 13)
	$gbAlign.Controls.Add($btnAlignBottom)
	$ToolTips.SetToolTip($btnAlignBottom, "Align selected controls to the bottom edge of`nthe bottommost control (press shift to`nalign to bottom of toptmost control)");

## size
	# --------------------------------------------------------
	#gbSize
	#
	$gbSize = New-Object Windows.Forms.GroupBox
	$gbSize.Location = New-Object System.Drawing.Point(203, 2)
	$gbSize.Size = New-Object System.Drawing.Size(104, 43)
	$gbSize.Text = "Size:"
	$frmPSFD.Controls.Add($gbSize)
	#
	#btnSizeWidth
	#
	$btnSizeWidth = New-Object System.Windows.Forms.Button
	$btnSizeWidth.Enabled = $false
	$btnSizeWidth.Text = "Width"
	$btnSizeWidth.Add_Click({Set-SizeWidthValues "Width" (Get-KeyState)})
	$btnSizeWidth.Size = New-Object System.Drawing.Size(44, 23)
	$btnSizeWidth.Location = New-Object System.Drawing.Point(8, 13)
	$gbSize.Controls.Add($btnSizeWidth)
	$ToolTips.SetToolTip($btnSizeWidth, "Set width of selected controls to the`nnarrowest control in the selection (press`nshift to size to the widest)");
	#
	#btnSizeHeight
	#
	$btnSizeHeight = New-Object System.Windows.Forms.Button
	$btnSizeHeight.Enabled = $false
	$btnSizeHeight.Text = "H'ght"
	$btnSizeHeight.Add_Click({Set-SizeWidthValues "Height" (Get-KeyState)})
	$btnSizeHeight.Size = New-Object System.Drawing.Size(44, 23)
	$btnSizeHeight.Location = New-Object System.Drawing.Point(52, 13)
	$gbSize.Controls.Add($btnSizeHeight)
	$ToolTips.SetToolTip($btnSizeHeight, "Set height of selected controls to the`nshortest control in the selection (press`nshift to size to the tallest)");

## space
	# --------------------------------------------------------
	#gbSpace
	#
	$gbSpace = New-Object Windows.Forms.GroupBox
	$gbSpace.Location = New-Object System.Drawing.Point(308, 2)
	$gbSpace.Size = New-Object System.Drawing.Size(190, 43)
	$gbSpace.Text = "Space:"
	$frmPSFD.Controls.Add($gbSpace)
	#
	#btnSpaceHorizontal
	#
	$btnSpaceHorizontal = New-Object System.Windows.Forms.Button
	$btnSpaceHorizontal.Enabled = $false
	$btnSpaceHorizontal.Text = "Horiz"
	$btnSpaceHorizontal.Add_Click({Space-Control "Horizontal" (Get-KeyState) 0})
	$btnSpaceHorizontal.Size = New-Object System.Drawing.Size(44, 23)
	$btnSpaceHorizontal.Location = New-Object System.Drawing.Point(8, 13)
	$gbSpace.Controls.Add($btnSpaceHorizontal)
	$ToolTips.SetToolTip($btnSpaceHorizontal, "Even the horizontal spacing of`nthe selected controls");
	#
	#btnSpaceVertical
	#
	$btnSpaceVertical = New-Object System.Windows.Forms.Button
	$btnSpaceVertical.Enabled = $false
	$btnSpaceVertical.Text = "Vert"
	$btnSpaceVertical.Add_Click({Space-Control "Vertical" (Get-KeyState) 0})
	$btnSpaceVertical.Size = New-Object System.Drawing.Size(44, 23)
	$btnSpaceVertical.Location = New-Object System.Drawing.Point(52, 13)
	$gbSpace.Controls.Add($btnSpaceVertical)
	$ToolTips.SetToolTip($btnSpaceVertical, "Even the vertical spacing of`nthe selected controls");
	#
	#btnMoreWidth
	#
	$btnMoreWidth = New-Object System.Windows.Forms.Button
	$btnMoreWidth.Enabled = $false
	$btnMoreWidth.Text = "Inc H Sp"
	$btnMoreWidth.Add_Click({Space-Control "Horizontal" (Get-KeyState) 1})
	$btnMoreWidth.Size = New-Object System.Drawing.Size(44, 23)
	$btnMoreWidth.Location = New-Object System.Drawing.Point(96, 13)
	$gbSpace.Controls.Add($btnMoreWidth)
	$ToolTips.SetToolTip($btnMoreWidth, "Increase (and even) the horizontal spacing`nof the selected controls (press`nshift to decrease)");
	#
	#btnMoreHeight
	#
	$btnMoreHeight = New-Object System.Windows.Forms.Button
	$btnMoreHeight.Enabled = $false
	$btnMoreHeight.Text = "Inc V Sp"
	$btnMoreHeight.Add_Click({Space-Control "Vertical" (Get-KeyState) 1})
	$btnMoreHeight.Size = New-Object System.Drawing.Size(44, 23)
	$btnMoreHeight.Location = New-Object System.Drawing.Point(140, 13)
	$gbSpace.Controls.Add($btnMoreHeight)
	$ToolTips.SetToolTip($btnMoreHeight, "Increase (and even) the vertial spacing`nof the selected controls (press`nshift to decrease)");
# endregion control controls


# region form control
	#
	#btnNewForm
	#
	$btnNewForm = New-Object System.Windows.Forms.Button
	$btnNewForm.Location = New-Object System.Drawing.Point(12, 52)
	$btnNewForm.Size = New-Object System.Drawing.Size(88, 23)
	$btnNewForm.Text = "New Form"
	$btnNewForm.Add_Click($btnNewForm_Click)
	$frmPSFD.Controls.Add($btnNewForm)
	## gpl ##
	$ToolTips.SetToolTip($btnNewForm, "Create New Form");
	#
	#btnOpenForm
	#
	$btnOpenForm = New-Object System.Windows.Forms.Button
	$btnOpenForm.Location = New-Object System.Drawing.Point(114, 52)
	$btnOpenForm.Size = New-Object System.Drawing.Size(88, 23)
	$btnOpenForm.Text = "Open Form"
	$btnOpenForm.Add_Click($btnOpenForm_Click)
	$frmPSFD.Controls.Add($btnOpenForm)
	## gpl ##
	$ToolTips.SetToolTip($btnOpenForm, "Open Existing Form");
	#
	#cbSkeleton
	#
	$cbSkeleton = New-Object System.Windows.Forms.CheckBox
	$cbSkeleton.Text = "Create Skeleton"
	$cbSkeleton.Size = New-Object System.Drawing.Size(104, 23)
	$cbSkeleton.Location = New-Object System.Drawing.Point(218, 56)
	$frmPSFD.Controls.Add($cbSkeleton)
	#
	#cbInclude
	#
	$cbInclude = New-Object System.Windows.Forms.CheckBox
	$cbInclude.Text = "Include 'Include line'"
	$cbInclude.Size = New-Object System.Drawing.Size(124, 23)
	$cbInclude.Location = New-Object System.Drawing.Point(326, 55)
	$frmPSFD.Controls.Add($cbInclude)
	#
	#btnSaveForm
	#
	$btnSaveForm = New-Object System.Windows.Forms.Button
	$btnSaveForm.Location = New-Object System.Drawing.Point(450, 52)
	$btnSaveForm.Size = New-Object System.Drawing.Size(88, 23)
	$btnSaveForm.Text = "Save Form"
	$btnSaveForm.Enabled = $false
	$btnSaveForm.Add_Click($btnSaveForm_Click)
	$frmPSFD.Controls.Add($btnSaveForm)
	## gpl ##
	$ToolTips.SetToolTip($btnSaveForm, "Save the Current Form");
	# --------------------------------------------------------
# endregion form control

# region controls

	#gbControls
	#
	$gbControls = New-Object Windows.Forms.GroupBox
	$gbControls.Location = New-Object System.Drawing.Point(12, 81)
	$gbControls.Size = New-Object System.Drawing.Size(260, 350)
	$gbControls.Text = "Controls:"
	$frmPSFD.Controls.Add($gbControls)
	#
	#cbAddControl
	#
	$cbAddControl = New-Object Windows.Forms.ComboBox
	$cbAddControl.Location = New-Object System.Drawing.Point(8, 14)
	$cbAddControl.Size = New-Object System.Drawing.Size(182, 21)
	$cbAddControl.Items.AddRange($AvailableControls)
	$gbControls.Controls.Add($cbAddControl)
	#
	#btnAddControl
	#
	$btnAddControl = New-Object System.Windows.Forms.Button
	$btnAddControl.Location = New-Object System.Drawing.Point(196, 14)
	$btnAddControl.Size = New-Object System.Drawing.Size(58, 23)
	$btnAddControl.Text = "Add"
	$btnAddControl.Add_Click($btnAddControl_Click)
	$btnAddControl.Enabled = $false
	$gbControls.Controls.Add($btnAddControl)
	## gpl ##
	$ToolTips.SetToolTip($btnAddControl, "Add a new Control");
	#
	#btnMoveUp
	#
	$btnMoveUp = New-Object System.Windows.Forms.Button
	$btnMoveUp.Enabled = $false
	$btnMoveUp.Text = "Move up"
	$btnMoveUp.Add_Click({MoveRow "Up"})
	$btnMoveUp.Size = New-Object System.Drawing.Size(58, 23)
	$btnMoveUp.Location = New-Object System.Drawing.Point(8, 43)
	$gbControls.Controls.Add($btnMoveUp)
	$ToolTips.SetToolTip($btnMoveUp, "Move the current control up in the list");
	#
	#btnClone
	#
	$btnClone = New-Object System.Windows.Forms.Button
	$btnClone.Enabled = $false
	$btnClone.Text = "Clone"
	$btnClone.Add_Click({CloneControl})
	$btnClone.Size = New-Object System.Drawing.Size(58, 23)
	$btnClone.Location = New-Object System.Drawing.Point(70, 43)
	$gbControls.Controls.Add($btnClone)
	$ToolTips.SetToolTip($btnClone, "Copy the current control note if it is`nin a container, you will need to amend`nthe generated source to show this");
	#
	#btnRemoveControl
	#
	$btnRemoveControl = New-Object System.Windows.Forms.Button
	$btnRemoveControl.Location = New-Object System.Drawing.Point(196, 43)
	$btnRemoveControl.Size = New-Object System.Drawing.Size(58, 23)
	$btnRemoveControl.Text = "Remove"
	$btnRemoveControl.Enabled = $false
	$btnRemoveControl.Add_Click({RemoveCurrentCtrl})
	$gbControls.Controls.Add($btnRemoveControl)
	## gpl ##
	$ToolTips.SetToolTip($btnRemoveControl, "Remove the current control");
	#
	#dgvControls
	#
	$dgvControls = New-Object System.Windows.Forms.DataGridView
	$dgvControls.Location = New-Object System.Drawing.Point(6, 72)
	$dgvControls.Size = New-Object System.Drawing.Size(248, 272)
	$null = $dgvControls.Columns.Add("", "Name")
	$null = $dgvControls.Columns.Add("", "Type")
	$null = $dgvControls.Columns.Add("", "LinkToControl")
	$dgvControls.Columns[0].Width = 159
	$dgvControls.Columns[1].Width = 86
	$dgvControls.Columns[2].Width = 0
	$dgvControls.Columns[0].ReadOnly = $true
	$dgvControls.Columns[1].ReadOnly = $true
	$dgvControls.ColumnHeadersHeightSizeMode = 'DisableResizing'
	$dgvControls.RowHeadersVisible = $false
	## gpl ##
	$dgvControls.MultiSelect = $true
	$dgvControls.ScrollBars = 'Vertical'
	$dgvControls.SelectionMode = 'FullRowSelect'
	$dgvControls.AllowUserToResizeRows = $false
	$dgvControls.AllowUserToAddRows = $false
	## gpl ##
	$dgvControls.Add_SelectionChanged($dgvControls_SelectionChanged)
	$dgvControls.Add_CellClick({SetCurrentCtrl $dgvControls.CurrentRow.Index})
	$gbControls.Controls.Add($dgvControls)

# endregion controls

# region properties
	#--------------------------------------------------------
	#gbProps
	#
	$gbProps = New-Object Windows.Forms.GroupBox
	$gbProps.Location = New-Object System.Drawing.Point(278, 81)
	$gbProps.Size = New-Object System.Drawing.Size(260, 350)
	$gbProps.Text = 'Properties:'
	$frmPSFD.Controls.Add($gbProps)
	#
	#cbAddProp
	#
	$cbAddProp = New-Object Windows.Forms.ComboBox
	## gpl ##
	$cbAddProp.Add_SelectedIndexChanged($cbAddProp_SelectedIndexChanged)
	$cbAddProp.Location = New-Object System.Drawing.Point(6, 14)
	$cbAddProp.Size = New-Object System.Drawing.Size(182, 21)
	$gbProps.Controls.Add($cbAddProp)
	#
	#btnAddProp
	#
	$btnAddProp = New-Object System.Windows.Forms.Button
	$btnAddProp.Location = New-Object System.Drawing.Point(194, 14)
	$btnAddProp.Size = New-Object System.Drawing.Size(58, 23)
	$btnAddProp.Text = "Add"
	$btnAddProp.Add_Click({AddProperty $cbAddProp.Text})
	$btnAddProp.Enabled = $false
	$gbProps.Controls.Add($btnAddProp)
	## gpl ##
	$ToolTips.SetToolTip($btnAddProp, "Add (or remove) a property");
	#
	#lblTooltip
	#
	$lblTooltip = New-Object System.Windows.Forms.Label
	$lblTooltip.Text = "Use Arrow keys and Ctrl to move and resize"
	$lblTooltip.Location = New-Object System.Drawing.Point(6, 44)
	$lblTooltip.Size = New-Object System.Drawing.Size(245, 16)
	$lblTooltip.Enabled = $false
	$gbProps.Controls.Add($lblTooltip)
	#
	#dgvProps
	#
	$dgvProps = New-Object System.Windows.Forms.DataGridView
	$dgvProps.Location = New-Object System.Drawing.Point(6, 70)
	$dgvProps.Size = New-Object System.Drawing.Size(248, 274)
	$null = $dgvProps.Columns.Add("", "Property")
	$null = $dgvProps.Columns.Add("", "Value")
	$dgvProps.Columns[0].Width = 75
	$dgvProps.Columns[1].Width = 170
	$dgvProps.Columns[0].ReadOnly = $true
	$dgvProps.ColumnHeadersHeightSizeMode = 'DisableResizing'
	$dgvProps.RowHeadersVisible = $false
	$dgvProps.AllowUserToResizeRows = $false
	$dgvProps.AllowUserToAddRows = $false
	$dgvProps.Add_CellEndEdit($dgvProps_CellEndEdit)
	$dgvProps.Add_DoubleClick($dgvProps_DoubleClick)
	$gbProps.Controls.Add($dgvProps)
	#
	#cbEnums
	#
	$cbEnums = New-Object System.Windows.Forms.ComboBox
	$cbEnums.Visible = $false
	$cbEnums.Size = New-Object System.Drawing.Size(182, 21)
	$cbEnums.Location = New-Object System.Drawing.Point(6, 14)
	## gpl ##
	$cbEnums.Add_SelectedIndexChanged($cbEnums_SelectedIndexChanged)
	$gbProps.Controls.Add($cbEnums)

# endregion properties

# region events
	#--------------------------------------------------------
	#gbEvents
	#
	$gbEvents = New-Object System.Windows.Forms.GroupBox
	$gbEvents.Text = "Event Handlers:"
	$gbEvents.Size = New-Object System.Drawing.Size(527, 114)
	$gbEvents.Location = New-Object System.Drawing.Point(12, 438)
	$frmPSFD.Controls.Add($gbEvents)
	#
	#cbEvents
	#
	$cbAddEvent = New-Object System.Windows.Forms.ComboBox
	$cbAddEvent.Size = New-Object System.Drawing.Size(182, 21)
	$cbAddEvent.Location = New-Object System.Drawing.Point(6, 14)
	## gpl ##
	$cbAddEvent.Add_SelectedIndexChanged($cbAddEvent_SelectedIndexChanged)
	$gbEvents.Controls.Add($cbAddEvent)
	#
	#btnAddEvent
	#
	$btnAddEvent = New-Object System.Windows.Forms.Button
	$btnAddEvent.Text = "Add"
	$btnAddEvent.Size = New-Object System.Drawing.Size(58, 23)
	$btnAddEvent.Location = New-Object System.Drawing.Point(197, 14)
	$btnAddEvent.Add_Click($btnAddEvent_Click)
	$btnAddEvent.Enabled = $false
	$gbEvents.Controls.Add($btnAddEvent)
	## gpl ##
	$ToolTips.SetToolTip($btnAddEvent, "Add (or remove) an event");
	#
	#dgvEvents
	#
	$dgvEvents = New-Object System.Windows.Forms.DataGridView
	$dgvEvents.Size = New-Object System.Drawing.Size(515, 66)
	$dgvEvents.Location = New-Object System.Drawing.Point(6, 43)
	$null = $dgvEvents.Columns.Add("", "Event")
	$null = $dgvEvents.Columns.Add("", "Handler")
	$dgvEvents.Columns[0].Width = 150
	$dgvEvents.Columns[1].Width = 375
	$dgvEvents.Columns[0].ReadOnly = $true
	$dgvEvents.ColumnHeadersVisible = $false
	$dgvEvents.RowHeadersVisible = $false
	$dgvEvents.AllowUserToResizeRows = $false
	$dgvEvents.AllowUserToAddRows = $false
	$dgvEvents.ScrollBars = 'Vertical'
	$dgvEvents.Add_CellEndEdit($dgvEvents_CellEndEdit)
	$gbEvents.Controls.Add($dgvEvents)
# endregion events

#--------------------------------------------------------
	[void]$frmPSFD.ShowDialog()
}
ShowMainWindow
